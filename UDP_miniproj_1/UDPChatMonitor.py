import sys
from socket import socket, SOCK_DGRAM, AF_INET, ntohs, htons, ntohl, htonl
import struct
from enum import Enum

chat_message_size = 8
sent_uid = []

class MsgType(Enum):
  CLIENT_HELLO = 1705
  CLIENT_MSG = 1706
  CLIENT_SET_NICKNAME = 1707
  CLIENT_BYEBYE = 1708
  SERVER_ACK = 1709
  SERVER_MSG = 1710
  MONITOR_HELLO = 1711
  SERVER_MONITOR_MESSAGE = 1712
  MONITOR_BYEBYE = 1713
  CLIENT_GET_MEMBERS = 1714

def print_error(e, f="UNKNOWN"):
    print("Error in %s!" % (f))
    print(e)
    print(type(e))

def send_monitor_hello(udp_sock, endpoint):

  try:

    # MONITOR_HELLO
    msg_type = MsgType.MONITOR_HELLO.value
    msg_len = 0
    uid = 100
    buf = struct.pack("!HHI", msg_type, msg_len, uid)
    ret = udp_sock.sendto(buf, endpoint)
    print("Sending HelloMessage with %d, len %d, uid %d" %(msg_type, msg_len, uid))
    print("Sent %d bytes" % (ret))

  except Exception as e:
    print_error(e, "sendto")


def send_monitor_bye(udp_sock, endpoint):

  try:

    # MONITOR_BYE
    msg_type = MsgType.MONITOR_BYEBYE.value
    msg_len = 0
    uid = 101
    buf = struct.pack("!HHI", msg_type, msg_len, uid)
    ret = udp_sock.sendto(buf, endpoint)
    print("Sending ByeMessage with %d, len %d, uid %d" %(msg_type, msg_len, uid))
    print("Sent %d bytes" % (ret))

  except Exception as e:
    print_error(e, "sendto")

def send_data(udp_sock, endpoint, data):
  try:
    ret = udp_sock.sendto(bytes(data, 'utf-8'), endpoint)
    print("Sent %d bytes" % (ret))
    return ret
  except Exception as e:
    print_error(e, "sendto")
    return 1


def recv_struct_data(udp_sock):
  try:
    data, (ip, port) = udp_sock.recvfrom(100)
    if len(data) > chat_message_size:
      type, msg_size, uid = struct.unpack("!HHI", data[0:chat_message_size])
      
      if type == MsgType.SERVER_MONITOR_MESSAGE.value:
        if uid not in sent_uid:
          if len(data) >= msg_size + chat_message_size:
            the_msg = data[chat_message_size:chat_message_size+msg_size]
            print(f"Received SERVER_MONITOR_MESSAGE with msg_len {msg_size}")
            message = the_msg.decode('utf-8')
            message = message.split(": ")
            print(message[1])
          else:
            print(f"Message size {len(data)} too small for ChatMessage")
            print("Received %d bytes" % (len(data)))
          sent_uid.append(uid)
        else:
            print(f"Ignoring already received message with uid {uid}")
      else:
        print(f"Wrong Message Type for ChatMessage")
  except Exception as e:
    print_error(e, "recvfrom")

def recv_data(udp_sock):
  try:
    data, (ip, port) = udp_sock.recvfrom(100)
    print("Received %d bytes" % (len(data)))
    print(data.decode('utf-8'))
    return data.decode('utf-8'), ip, port
  except Exception as e:
    print_error(e, "recvfrom")
    return None, None, None


def main():
  if len(sys.argv) == 3:
    ip = sys.argv[1]
    try:
      port = int(sys.argv[2])
    except:
      print("Port %s unable to be converted to number, run with HOST PORT" % (sys.argv[2]))
      sys.exit(1)
  else:
    print("Run with %s HOST PORT" % (sys.argv[0]))
    sys.exit(1)

  try:
    udp_sock = socket(AF_INET, SOCK_DGRAM)
    send_monitor_hello(udp_sock, (ip, port))
  except Exception as e:
    print_error(e, "socket")
  
  check = True
  while check:
    try:
      current_uid = recv_struct_data(udp_sock)
    except KeyboardInterrupt:
      send_monitor_bye(udp_sock, (ip, port))
      udp_sock.close()
      check = False
    except Exception as e:
      print_error(e, "recv_data")
      udp_sock.close()
      check = False


if __name__ == "__main__":
  main()
