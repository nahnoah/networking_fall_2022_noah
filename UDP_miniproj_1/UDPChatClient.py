import sys
from socket import socket, SOCK_DGRAM, AF_INET
import struct
from enum import Enum
 
class MsgType(Enum):
  CLIENT_HELLO = 1705
  CLIENT_MSG = 1706
  CLIENT_SET_NICKNAME = 1707
  CLIENT_BYEBYE = 1708
  SERVER_ACK = 1709
  SERVER_MSG = 1710



def print_error(e, f="UNKNOWN"):
    print("Error in %s!" % (f))
    print(e)
    print(type(e))


def send_struct_data(udp_sock, endpoint, data):

  try:

    # CLIENT_HELLO
    msg_type = MsgType.CLIENT_HELLO.value
    data_bytes = bytes(data, 'utf-8')
    msg_len = 0
    uid = 0
    buf = struct.pack("!HHI", msg_type, msg_len, uid)
    ret = udp_sock.sendto(buf, endpoint)
    print("Sending ChatMessage with %d, len %d, uid %d" %(msg_type, msg_len, uid))
    print("Sent %d bytes" % (ret))


    # CLIENT_MSG
    msg_type = MsgType.CLIENT_MSG.value
    data_bytes = bytes(data, 'utf-8')
    msg_len = len(data_bytes)
    uid += 1
    buf = struct.pack("!HHI", msg_type, msg_len, uid)
    send_buf = buf + bytes(data, 'utf-8')
    ret = udp_sock.sendto(send_buf, endpoint)
    print("Sending ChatMessage with %d, len %d, uid %d" %(msg_type, msg_len, uid))
    print("Sent %d bytes" % (ret))

    # CLIENT_BYEBYE
    msg_type = MsgType.CLIENT_BYEBYE.value
    data_bytes = bytes(data, 'utf-8')
    msg_len = 0
    uid += 1
    buf = struct.pack("!HHI", msg_type, msg_len, uid)
    ret = udp_sock.sendto(buf, endpoint)
    print("Sending ChatMessage with %d, len %d, uid %d" %(msg_type, msg_len, uid))
    print("Sent %d bytes" % (ret))

  except Exception as e:
    print_error(e, "sendto")


def send_data(udp_sock, endpoint, data):
  try:
    ret = udp_sock.sendto(bytes(data, 'utf-8'), endpoint)
    print("Sent %d bytes" % (ret))
  except Exception as e:
    print_error(e, "sendto")


def main():
  # print(len(sys.argv))
  if len(sys.argv) >= 3:
    ip = sys.argv[1]
    try:
      port = int(sys.argv[2])
    except:
      print("Port %s unable to be converted to number, run with HOST PORT" % (sys.argv[2]))
      sys.exit(1)

  data = None
  if len(sys.argv) == 4:
    data = sys.argv[3]
    print("Will send %s to %s:%d via udp" % (data, ip, port))
  else:
    print("Must enter data to send as argument to program")

  try:
    udp_sock = socket(AF_INET, SOCK_DGRAM)
    send_struct_data(udp_sock, (ip, port), data)
  except Exception as e:
    print_error(e, "socket")

if __name__ == "__main__":
  main()
