#!/opt/local/bin/python3.7

import sys
from socket import socket, SOCK_STREAM, AF_INET, inet_pton, inet_ntop
from select import select
import struct
import traceback
from enum import IntEnum
import time
from datetime import datetime

class MsgType(IntEnum):
  
  CONTROL_MSG = 333
  DATA_MSG = 334
  ERROR_MSG = 335
  INCORRECT_MESSAGE_TYPE = 20
  INCORRECT_MESSAGE_SIZE = 21
  CONNECT= 1223
  CONNECT_OK = 1224
  DISCONNECT = 1225
  FIND_PEERS = 1226
  GOSSIP_PEERS = 1227


connected_clients = []
known_clients = []
client_list = []
read_sockets = []
write_sockets = []
except_sockets = []

class Client:
    def __init__(self, socket=None, ip=None, port=None):
      self.socket = socket
      self.ip = ip
      self.port = port

def print_error(e, f="UNKNOWN"):
    print("Error in %s!" % (f))
    print(e)
    print(type(e))
    traceback.print_exc()

def recv_message(tcp_sock, server_port, server_ip):
  try:
    data = tcp_sock.recv(4096)
    if len(data) == 0:
      return (False, None, None)

    elif len(data) == 28:

      # receive message  
      (ctrl_msg, msg_len, msg, port, binary_ip, a, b, c, d) = struct.unpack("!HHHH4sllll", data)
      ip = inet_ntop(AF_INET, binary_ip)

      # if it is a connect message, send connect ok message
      if msg == MsgType.CONNECT.value:
        send_connect_ok_msg(tcp_sock, server_port, server_ip)
        return (True, ip, port)

      # if it is a connect ok message, send find peers message
      elif msg == MsgType.CONNECT_OK.value:
        send_to_ip, send_to_port = tcp_sock.getpeername()
        send_find_peers_msg(tcp_sock, server_port, server_ip)
        return (True, send_to_ip, send_to_port)
      
    # if it is a find peer message, send gossip peers
    elif len(data) == 10:
      (ctrl_msg, msg_len, msg, binary_ip) = struct.unpack("!HHH4s", data)
      ip = inet_ntop(AF_INET, binary_ip)
      (peer_ip, peer_port) = available_peer(str(ip))
      if peer_ip != None and peer_port != None:
        send_gossip_peers_msg(tcp_sock, peer_port, peer_ip)
      return (True, None, None)

    # if it is a gossip peer message, send connect message to peer and add to client list
    elif len(data) == 30:
      (ctrl_msg, msg_len, msg, no_peers, port, binary_ip, a, b, c, d) = struct.unpack("!HHHHH4sllll", data)
      ip = inet_ntop(AF_INET, binary_ip)
      tcp_client = Client(socket = socket(AF_INET))
      tcp_client.socket.connect((ip, port))

      if ip != server_ip and port != server_port:
        if not check_client_exist_in_client_list(ip, port, server_ip, server_port):
          (tcp_client.ip, tcp_client.port) = send_connect_msg(tcp_client.socket, port, ip)
          client_list.append(tcp_client)
      return (True, ip, port)

  except BlockingIOError as b:
    print_error(b, "Recv failed due to non-blocking IO, this means the client has disconnected?")
    return (False, None, None)
  except struct.error as s:
    print_error(s, "Unable to unpack struct")
    return (False, None, None)
  except Exception as e:
    print_error(e, "recv")
    return (False, None, None)

def send_connect_msg(tcp_sock, port, ip):

  try:
    connect_msg_type = MsgType.CONNECT.value
    cntrl_msg_type = MsgType.CONTROL_MSG.value
    msg_len = 28
    buf = struct.pack("!HHHH4sllll", cntrl_msg_type, msg_len, connect_msg_type, int(port), inet_pton(AF_INET, str(ip)), 0,0,0,0)
    tcp_sock.send(buf)
    send_to_ip, send_to_port = tcp_sock.getpeername()
    return (send_to_ip, send_to_port)
  except KeyboardInterrupt as k:
    raise KeyboardInterrupt()
  except Exception as e:
    print_error(e, "send")

def send_connect_ok_msg(tcp_sock, port, ip):
  try:
    connect_msg_type = MsgType.CONNECT_OK.value
    cntrl_msg_type = MsgType.CONTROL_MSG.value
    msg_len = 28
    buf = struct.pack("!HHHH4sllll", cntrl_msg_type, msg_len, connect_msg_type, int(port), inet_pton(AF_INET, str(ip)), 0,0,0,0)
    ret = tcp_sock.send(buf)
    return ret
  except KeyboardInterrupt as k:
    raise KeyboardInterrupt()
  except Exception as e:
    print_error(e, "send")

def send_find_peers_msg(tcp_sock, port, ip):
  try:
    connect_msg_type = MsgType.FIND_PEERS.value
    cntrl_msg_type = MsgType.CONTROL_MSG.value
    msg_len = 10
    buf = struct.pack("!HHH4s", cntrl_msg_type, msg_len, connect_msg_type, inet_pton(AF_INET, str(ip)))
    ret = tcp_sock.send(buf)
    return ret
  except KeyboardInterrupt as k:
    raise KeyboardInterrupt()
  except Exception as e:
    print_error(e, "send")

def send_gossip_peers_msg(tcp_sock, port, ip):
  try:
    connect_msg_type = MsgType.GOSSIP_PEERS.value
    cntrl_msg_type = MsgType.CONTROL_MSG.value
    msg_len = 30
    peer_len = 1
    buf = struct.pack("!HHHHH4sllll",cntrl_msg_type, msg_len, connect_msg_type, peer_len, int(port), inet_pton(AF_INET, str(ip)), 0, 0, 0, 0)
    ret = tcp_sock.send(buf)
    return ret
  except KeyboardInterrupt as k:
    raise KeyboardInterrupt()
  except Exception as e:
    print_error(e, "send")


def check_client_exist_in_client_list(ip, port, seed_ip, seed_port):
  for client in client_list:
    if client.ip == ip and client.port == port or (client.ip == seed_ip and client.port == seed_port):
      return True
  return False

def check_client_exist_in_connected_list(ip, port, seed_ip, seed_port):
  for client in connected_clients:
    if client.ip == ip and client.port == port or (client.ip == seed_ip and client.port == seed_port):
      return True
  return False

def check_client_exist_in_known_list(ip, port, seed_ip, seed_port):
  for client in known_clients:
    if client.ip == ip and client.port == port or (client.ip == seed_ip and client.port == seed_port):
      return True
  return False

def available_peer(ip):
  for client in connected_clients:
    if client.ip != ip:
      return (client.ip, client.port)
  return (None, None)


def main():

  # initialize client_ip and client_port
  client_ip = ""
  client_port = ""

  # validate commandline arguments
  if len(sys.argv) == 3:
    ip = sys.argv[1]
    try:
      port = int(sys.argv[2])
    except:
      print("Port %s unable to be converted to number, run with HOST PORT" % (sys.argv[2]))
      sys.exit(1)
  
  # if seed ip and seed port exists
  elif len(sys.argv) == 5:
    ip = sys.argv[1]
    client_ip = sys.argv[3]
    try:
      port = int(sys.argv[2])
      client_port = int(sys.argv[4])
    except:
      print("Port %s unable to be converted to number, run with HOST PORT" % (sys.argv[4]))
      sys.exit(1)
  
  else:
    print("Run with %s HOST PORT" % (sys.argv[0]))
    sys.exit(1)

  try:
    server_sock = socket(AF_INET, SOCK_STREAM)
  except Exception as e:
    print_error(e, "socket")
    sys.exit(1)
  
  try:
    server_sock.bind((ip, port))
  except Exception as e:
    print_error(e, "bind")
    sys.exit(1)

  try:
    server_sock.listen(10)
  except Exception as e:
    print_error(e, "listen")
    sys.exit(1)


  # if seed ip and seed port exists
  if client_ip and client_port:
    try:
      tcp_client = Client(socket = socket(AF_INET))
    except Exception as e:
      print_error(e, "socket")

    try:
      # connect socket to seed ip and seed port
      tcp_client.socket.connect((client_ip, client_port))
      # sends connect message to seed before appending to read socket
      (tcp_client.ip, tcp_client.port) = send_connect_msg(tcp_client.socket, port, ip)
    except Exception as e:
      print_error(e, "connect")

  # if seed ip and port exist, append to client_list
  if client_ip and client_port:
    client_list.append(tcp_client)

  read_sockets.append(server_sock)

  except_sockets.append(server_sock)
  read_sockets.append(sys.stdin)
  quit = False

  start_time = datetime.now()

  print("Enter Command: ")

  while (quit == False):
    readlist, writelist, exceptlist = [], [], []

    for client in client_list:
      if client.socket not in read_sockets:
        read_sockets.append(client.socket)
      continue

    try:
      readlist, writelist, exceptlist = select(read_sockets, write_sockets, except_sockets, 2)
    except ValueError as v:
      print_error(v, "select")
    except KeyboardInterrupt as k:
      quit = True
    except Exception as e:
      print_error(e, "select")

    # send find peers to clients periodically
    now = datetime.now()
    time_dif = now - start_time
    if time_dif.total_seconds() >= 3:
      for client in connected_clients:
        send_find_peers_msg(client.socket, port, ip)
      start_time = now

    if server_sock in readlist:
      try:
        # accept new client connection
        client_sock, (client_ip, client_port) = server_sock.accept()
        client_sock.setblocking(0)
        read_sockets.append(client_sock)
        except_sockets.append(client_sock)
        
        new_client = Client(ip=client_ip, port=client_port, socket=client_sock)
        if new_client not in client_list:
          client_list.append(new_client)
        continue
      except KeyboardInterrupt as k:
        quit = True
      except Exception as e:
        print_error(e, "accept")

    for c in client_list:
      # means client has sent us data
      if c.socket in readlist:
        try:
          (ret_state, ret_ip, ret_port) = recv_message(c.socket, port, ip)
          if ret_state == True:
            if c.ip == ret_ip:
              c.port = ret_port
            if not check_client_exist_in_connected_list(c.ip, c.port, ip, port):
              connected_clients.append(c)
            if not check_client_exist_in_known_list(c.ip, c.port, ip, port):
              known_clients.append(c)
          elif ret_state == False:
            if check_client_exist_in_connected_list(c.ip, c.port, ip, port):
              connected_clients.remove(c)
        except KeyboardInterrupt as k:
          quit = True
        except Exception as e:
          print_error(e, "recv_data")

      if c.socket in exceptlist:
        print("Closing client socket (client in except?).")
        c.socket.close()
        read_sockets.remove(c.socket)
        except_sockets.remove(c.socket)

    if sys.stdin in readlist:
      command = sys.stdin.readline().strip()
      if(command == "LIST_PEERS"):
        print("LIST_PEERS received")
        print("Currently have %d peer(s) connected" %(len(connected_clients)))
        count = 0
        for client in connected_clients:
          client_address = str(client.ip) +":" + str(client.port)
          print("Peer %d client listen address: %s" %(count, client_address))
          count+=1

      elif(command == "quit"):
        for client_sock in read_sockets:
          client_sock.close()
        quit = True
      else:
        continue
    
  try:
    print("Closing sockets...")
    server_sock.close()
  except:
    pass
if __name__ == "__main__":
  main()
